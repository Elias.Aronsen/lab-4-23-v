package no.uib.inf101.bonus;

import javax.swing.JPanel;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Line2D;
import java.awt.geom.Ellipse2D;
import java.awt.image.BufferedImage;

public class BeautifulPicture extends JPanel{

  public BeautifulPicture(){
    this.setPreferredSize(new Dimension(1200, 600));
  }

  
  @Override
  public void paintComponent(Graphics g) {
    super.paintComponent(g);
    Graphics2D g2 = (Graphics2D) g;

    g2.setBackground(Color.BLACK);
    BufferedImage image1 = Inf101Graphics.loadImageFromResources("/sondre.jpg");
    BufferedImage image2 = Inf101Graphics.loadImageFromResources("/torstein.jpg");
    BufferedImage image3 = Inf101Graphics.loadImageFromResources("/martin.jpg");
    g2.setColor(Color.RED);


    double x = 50;
    double y = 80;
    //adds the images  
    Inf101Graphics.drawImage(g2, image1, x, y, 0.75);
    x += image1.getWidth();
    Inf101Graphics.drawImage(g2, image2, x, y, 0.75, 0.25);
    x += image2.getWidth();
    Inf101Graphics.drawImage(g2, image3, x, y, 0.75);

    //draws shine-lines

    double xS = 155;
    double yS = 280;

    double xE = 220;
    double yE = 280;


    //sondre
    g2.setColor(Color.RED);
    g2.fill(new Ellipse2D.Double(xS,yS-7,65,15));


    g2.setColor(Color.RED);
    g2.fill(new Ellipse2D.Double(xS + (xE-xS)/2 - 16,yS-10,15,10));
    g2.setColor(Color.RED);
    g2.fill(new Ellipse2D.Double(xS + (xE-xS)/2,yS-10,15,10));
    g2.setColor(Color.RED);
    g2.fill(new Ellipse2D.Double(xS + (xE-xS)/2 -10,yS,20,10));

    g2.setColor(Color.PINK);
    g2.draw(new Line2D.Double(xS + 2, yS +1, xE -2, yE -1));

    //eyes
    g2.setColor(Color.BLACK);
    g2.fill(new Ellipse2D.Double(xS - 20,yS-80, 40,40));
    g2.setColor(Color.WHITE);
    g2.fill(new Ellipse2D.Double(xS - 17,yS-77, 34,34));

    g2.setColor(Color.BLACK);
    g2.fill(new Ellipse2D.Double(xS,yS-65, 10,10));


    g2.setColor(Color.BLACK);
    g2.fill(new Ellipse2D.Double(xS + 45,yS-80, 40,40));
    g2.setColor(Color.WHITE);
    g2.fill(new Ellipse2D.Double(xS + 48,yS-77, 34,34));

    g2.setColor(Color.BLACK);
    g2.fill(new Ellipse2D.Double(xS + 55,yS-65, 20,20));



    

    //torstein

    xS = 155 +445;
    yS = 270;

    xE = 200+445;
    yE = 270;

    g2.setColor(Color.RED);
    g2.fill(new Ellipse2D.Double(xS,yS-7,xE-xS,15));


    g2.setColor(Color.RED);
    g2.fill(new Ellipse2D.Double(xS + (xE-xS)/2 - 13,yS-10,15,10));
    g2.setColor(Color.RED);
    g2.fill(new Ellipse2D.Double(xS + (xE-xS)/2 -3,yS-10,15,10));
    g2.setColor(Color.RED);
    g2.fill(new Ellipse2D.Double(xS + (xE-xS)/2 -10,yS,20,10));

    g2.setColor(Color.PINK);
    g2.draw(new Line2D.Double(xS + 2, yS +1, xE -2, yE -1));


    //eyes
    g2.setColor(Color.BLACK);
    g2.fill(new Ellipse2D.Double(xS - 20,yS-60, 40,40));
    g2.setColor(Color.WHITE);
    g2.fill(new Ellipse2D.Double(xS - 17,yS-57, 34,34));

    g2.setColor(Color.BLACK);
    g2.fill(new Ellipse2D.Double(xS,yS-40, 10,10));


    g2.setColor(Color.BLACK);
    g2.fill(new Ellipse2D.Double(xS + 35,yS-60, 40,40));
    g2.setColor(Color.WHITE);
    g2.fill(new Ellipse2D.Double(xS + 38,yS-57, 34,34));

    g2.setColor(Color.BLACK);
    g2.fill(new Ellipse2D.Double(xS + 45,yS-45, 7,7));



    //martin

    xS = 153 +832;
    yS = 187;

    xE = 185+832;
    yE = 187;

    g2.setColor(Color.RED);
    g2.fill(new Ellipse2D.Double(xS,yS-7,xE-xS,10));

    g2.setColor(Color.PINK);
    g2.draw(new Line2D.Double(xS + 2, yS -2, xE -2, yE-2));



    //eyes
    g2.setColor(Color.BLACK);
    g2.fill(new Ellipse2D.Double(xS - 5,yS-35, 20,20));
    g2.setColor(Color.WHITE);
    g2.fill(new Ellipse2D.Double(xS - 2,yS-32, 14,14));

    g2.setColor(Color.BLACK);
    g2.fill(new Ellipse2D.Double(xS+2,yS-27, 5,5));


    g2.setColor(Color.BLACK);
    g2.fill(new Ellipse2D.Double(xS + 20,yS-35, 20,20));
    g2.setColor(Color.WHITE);
    g2.fill(new Ellipse2D.Double(xS + 23,yS-32, 14,14));

    g2.setColor(Color.BLACK);
    g2.fill(new Ellipse2D.Double(xS + 25,yS-28, 7,7));





  }

}
