package no.uib.inf101.bonus;

import java.awt.Color;

import javax.swing.JFrame;

public class Main {
  public static void main(String[] args) {
    // Kopier inn main-metoden fra kursnotatene om grafikk her,
    // men tilpass den slik at du oppretter et BeautifulPicture -objekt
    // som lerret.
    
    BeautifulPicture canvas = new BeautifulPicture(); 
    JFrame frame = new JFrame();

    frame.setContentPane(canvas);
    frame.setTitle("Beautiful Picture");
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.pack();
    frame.setVisible(true);
    frame.getContentPane().setBackground(Color.BLACK);
  }
}
