package no.uib.inf101.gridview;

import java.awt.geom.Rectangle2D;
import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.GridDimension;

public class CellPositionToPixelConverter {
  
  Rectangle2D box;  // Box which contains grid
  GridDimension gd; // The size of the grid, by rows and cols
  double margin;    // The amount of space between cells

  public CellPositionToPixelConverter(Rectangle2D box, GridDimension gd, double margin){
    this.box = box;
    this.gd = gd;
    this.margin = margin;
  }

  public Rectangle2D getBoundsForCell(CellPosition cp){
    
    //width and X
    double PixelsForCellsWidth = box.getWidth() - (gd.cols() + 1)*margin; // returns total amount of pixels for cellsWidth
    double cellWidth = PixelsForCellsWidth/gd.cols();
    double cellX = box.getX() + margin + cp.col()*(cellWidth + margin);

    //height and Y
    double PixelsForCellsHeight = box.getHeight() - (gd.rows() + 1)*margin; // returns total amount of pixels for cellsHeight
    double cellHeight = PixelsForCellsHeight/gd.rows();
    double cellY = box.getY() + margin + cp.row()*(cellHeight + margin);

    return new Rectangle2D.Double(cellX, cellY, cellWidth, cellHeight);
  }

}
