package no.uib.inf101.gridview;

import java.awt.Dimension;

import javax.swing.JPanel;

import no.uib.inf101.colorgrid.CellColor;
import no.uib.inf101.colorgrid.CellColorCollection;
import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.IColorGrid;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.util.List;

public class GridView extends JPanel{

  IColorGrid grid;

  private static final Color MARGINCOLOR = Color.LIGHT_GRAY;
  private static final double OUTERMARGIN = 30;
  private static final double INNERMARGIN = 30;
  

  public GridView( IColorGrid grid) {
    this.setPreferredSize(new Dimension(400, 300));
    this.grid = grid;
  }

  @Override
  public void paintComponent(Graphics g) {
    super.paintComponent(g);
    Graphics2D g2 = (Graphics2D) g;

    drawGrid(g2);
  }

  public void drawGrid(Graphics2D g2){
    //draw background box with margin 30 
    Rectangle2D box = new Rectangle2D.Double(OUTERMARGIN,OUTERMARGIN, 340, 240);
    g2.setColor(MARGINCOLOR);
    g2.fill(box);

    CellPositionToPixelConverter converter = new CellPositionToPixelConverter(box, grid, INNERMARGIN);

    drawCells(g2,grid,converter);

  }

  static private void drawCells(Graphics2D g2, CellColorCollection grid, CellPositionToPixelConverter converter){

    List<CellColor> cells = grid.getCells(); //gets list of cells in grid

    for(int i = 0; i < cells.size(); i++){ // loops trough all cells in grid

      CellPosition cellPos = cells.get(i).cellPosition(); // finds coordinates for specific cell in the form of (rows, column)
      Rectangle2D cell = converter.getBoundsForCell(cellPos); // defines a cell as a rectangle with specific coordinates according to converter 

      if(cells.get(i).color() == null){
        g2.setColor(Color.DARK_GRAY); // sets basic cell color as dark gray
      } else{
        g2.setColor(cells.get(i).color()); // sets color for the cell according to what specific cell it is
      }
      g2.fill(cell); // colors cell
      
    }
  }
}
