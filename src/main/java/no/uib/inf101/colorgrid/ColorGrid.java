package no.uib.inf101.colorgrid;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

public class ColorGrid implements IColorGrid{

  int rows;
  int cols;

  ArrayList<CellColor> grid;

  public ColorGrid(int rows, int cols){
    if(rows<=0) {
			throw new IllegalArgumentException("Illegal amount of rows: " + rows);
		}
		if(cols<=0) {
			throw new IllegalArgumentException("Illegal amount of columns: " + cols);
		}

    this.rows = rows;
    this.cols = cols;
    grid = new ArrayList<CellColor>();
    for(int r = 0; r < rows; r++){
      for(int c = 0; c < cols; c++){
        grid.add(new CellColor(new CellPosition(r, c), null));
      }
    }
  }

  @Override
  public int rows() {
    return rows;
  }

  @Override
  public int cols() {
    return cols;
  }

  @Override
  public List<CellColor> getCells() {
    return grid;
  }

  @Override
  public Color get(CellPosition pos) { // IColorGrid grid = new WColorGrid(10, 20);
    if(pos.row() > (rows()-1) || pos.row() < 0) {
			throw new IndexOutOfBoundsException("Illegal amount of rows");
		}
		if(pos.col() > (cols()-1) || pos.col() < 0) {
			throw new IndexOutOfBoundsException("Illegal amount of columns");
		}

    int posIndex= indexOf(pos.row(), pos.col());
    return grid.get(posIndex).color();
  }

  @Override
  public void set(CellPosition pos, Color color) {
    if(pos.row() > (rows()-1) || pos.row() < 0) {
			throw new IndexOutOfBoundsException("Illegal amount of rows");
		}
		if(pos.col() > (cols()-1) || pos.col() < 0) {
			throw new IndexOutOfBoundsException("Illegal amount of columns");
		}

    int posIndex = indexOf(pos.row(), pos.col());
    grid.set(posIndex, new CellColor(pos, color));   
  }

  public Integer indexOf(int row, int col){ // finds the index of the cell in the list accordind to which column and row it is on
    if(row < 0 || row > rows()-1)
			throw new IndexOutOfBoundsException("Illegal row value");
		if(col < 0 || col > cols()-1)
			throw new IndexOutOfBoundsException("Illegal column value");
    return row*cols() + col;
  }
}
